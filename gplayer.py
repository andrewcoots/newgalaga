from turtle import Turtle, Screen
import gscreen

player_color = 'blue'
player_shape = 'classic'
player_speed = 50  # Guess it's a good value?
player_face = 90  # Which way ship "nose" points
player_startx, player_starty = 0, -200  # Place in lower end of frame


class Player:
    player = Turtle()

    # Drawn (visible) attributes
    player.pendown()
    player.color(player_color)
    player.shape(player_shape)
    player.shapesize(2, 2)
    player.penup()

    # Non-visible attributes
    player.speed(0)  # Start game w/out moving
    player.setposition(player_startx, player_starty)
    player.setheading(player_face)


# Player movements
def move_left():
    global player
    playerx = player.xcor() - player_speed

    if playerx < -gscreen.screen_width / 2:
        playerx = -gscreen.screen_width/2

    player.setx(playerx)

    def move_right():
        playerx = player.xcor() + player_speed

        if playerx > gscreen.screen_width / 2:
            playerx = gscreen.screen_width/2

        player.setx(playerx)
