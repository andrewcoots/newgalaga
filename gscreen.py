from turtle import Screen

# Screen attributes
screen_height = 600
screen_width = 600
screen_border = 80
screen_bg = "space.gif"


class GScreen:
    thisScreen = Screen()
    thisScreen.setup(
        screen_height, screen_width,  0, 0)
    thisScreen.bgpic(screen_bg)
