from turtle import Turtle
from random import randint


class Raider:
    raider = Turtle()

    # Drawn (visible) attributes
    raider.color("red")
    raider.shape("classic")
    raider.shapesize(3, 3)
    raider.penup()

    # Non-visible attributes
    raider.speed(10)  # Spin from center into postion
    raider.setheading(270)  # Point the "nose" down
    x_position = randint(-200, 200)
    y_position = randint(100, 250)
