#!/usr/bin/env python3

from turtle import Screen
# from random import randint
import gscreen
import gplayer
import graider

danger_zone = 15  # Distance until two things hit
number_of_enemies = 5
player_fire_speed = 20
enemy_fire_speed = 5

window = gscreen.GScreen()
ship = gplayer.Player()
raider = graider.Raider()

screen = Screen()
screen.listen()

screen.mainloop()
