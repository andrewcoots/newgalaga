from turtle import Turtle, Screen
from random import randint

# Basic Config
NUMBER_OF_ENEMIES = 5
ENEMY_JUMP = 40

BULLET_SPEED = 20
PLAYER_SPEED = 15

SAFETY_DISTANCE = 15

GALLERY_WIDTH, GALLERY_HEIGHT = 560, 550
GALLERY_BORDER = 80


# player movement
def move_left():
    x = player.xcor() - PLAYER_SPEED

    if x < -GALLERY_WIDTH / 2:
        x = -GALLERY_WIDTH / 2

    player.setx(x)


def move_right():
    x = player.xcor() + PLAYER_SPEED

    if x > GALLERY_WIDTH / 2:
        x = GALLERY_WIDTH / 2

    player.setx(x)


def fire_bullet():
    global bulletstate

    if bulletstate == 'ready':
        bulletstate = 'fire'
        bullet.setposition(player.position())
        bullet.forward(BULLET_SPEED / 2)
        bullet.showturtle()


def isCollision(t1, t2):
    return t1.distance(t2) < SAFETY_DISTANCE


def move():
    global enemy_speed, bulletstate

    screen.tracer(False)

    for enemy in enemies:
        x = enemy.xcor() + enemy_speed

        enemy.setx(x)

        if x > GALLERY_WIDTH / 2:
            for e in enemies:
                y = e.ycor() - ENEMY_JUMP
                e.sety(y)

            enemy_speed *= -1

        elif x < -GALLERY_WIDTH / 2:
            for e in enemies:
                y = e.ycor() - ENEMY_JUMP
                e.sety(y)

            enemy_speed *= -1

        if isCollision(player, enemy):
            player.hideturtle()
            enemy.hideturtle()
            print("Get To The Doctors! You're Riddled!")
            screen.update()
            return

        if isCollision(bullet, enemy):
            # reset the bullet
            bullet.hideturtle()
            bullet.setposition(0, -GALLERY_HEIGHT)
            bulletstate = 'ready'

            # reset the enemy
            enemy.hideturtle()
            x = randint(GALLERY_BORDER - GALLERY_WIDTH / 2, GALLERY_WIDTH / 2 - GALLERY_BORDER)
            y = randint(GALLERY_HEIGHT / 2 - 175, GALLERY_HEIGHT / 2 - 25)
            enemy.setposition(x, y)
            enemy.showturtle()

    if bulletstate == 'fire':
        bullet.forward(BULLET_SPEED)

        # check to see if the bullets has gone to the top
        if bullet.ycor() > GALLERY_HEIGHT / 2:
            bullet.hideturtle()
            bulletstate = 'ready'

    screen.tracer(True)

    screen.ontimer(move, 25)


screen = Screen()
screen.setup(GALLERY_WIDTH + GALLERY_BORDER, GALLERY_HEIGHT + GALLERY_BORDER)
screen.bgcolor('black')

player = Turtle('turtle', visible=False)
player.speed('fastest')
player.color('red')
player.penup()
player.setheading(90)
player.sety(-250)  # need to define this
player.showturtle()

enemies = []

enemy_speed = 1

for _ in range(NUMBER_OF_ENEMIES):
    enemy = Turtle('circle', visible=False)
    enemy.speed('fastest')
    enemy.color('green')
    enemy.penup()

    x = randint(
        GALLERY_BORDER - GALLERY_WIDTH / 2, GALLERY_WIDTH / 2 - GALLERY_BORDER)
    y = randint(GALLERY_HEIGHT / 2 - 175, GALLERY_HEIGHT / 2 - 25)  # define these!
    enemy.setposition(x, y)
    enemy.showturtle()

    enemies.append(enemy)

# create the player's spunk shots
bullet = Turtle('triangle', visible=False)
bullet.speed('fastest')
bullet.shapesize(0.5)
bullet.color('white')
bullet.penup()
bullet.setheading(90)

# define bullet state
# ready - ready to fire
# fire - bullet is firing
bulletstate = 'ready'

screen.onkey(move_left, 'Left')
screen.onkey(move_right, 'Right')
screen.onkey(fire_bullet, 'space')

screen.listen()

move()

screen.mainloop()
