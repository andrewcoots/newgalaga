from turtle import Turtle, Screen
import math
import random
# import os

# Setting up the screen
wn = Screen()
wn.bgpic("space.gif")
wn.title("Galaga")

# Drawing the border
border_pen = Turtle()
border_pen.speed(0)
border_pen.color("white")
border_pen.penup()
border_pen.setposition(-300, -300)
border_pen.pendown()
border_pen.pensize(3)
for side in range(4):
    border_pen.fd(600)
border_pen.lt(90)
border_pen.hideturtle()

# Create Player
player = Turtle()
player.color("blue")
player.shape("classic")
player.shapesize(2, 2)
player.penup()
player.speed(0)
# Placing in lower end of frame
player.setposition(0, -200)
# Turning "turtle" upright
player.setheading(90)

playerspeed = 70

# Enemy
number_of_enemies = 5
enemies = []

enemyspeed = 2

for i in range(number_of_enemies):
    enemy = Turtle()
    enemy.color("red")
    enemy.shape("classic")
    enemy.shapesize(3, 3)
    enemy.penup()
    enemy.speed(10)
    enemy.setheading(270)
    x = random.randint(-200, 200)
    y = random.randint(100, 250)
    enemy.setposition(x, y)
    enemy.showturtle()


# Player's Bullet
bullet = Turtle()
bullet.color("yellow")
bullet.shape("triangle")
bullet.penup()
bullet.speed(0)
bullet.setheading(90)
bullet.shapesize(0.5, 0.5)
bullet.hideturtle()

bulletspeed = 50

# Defining a bullet state
# ready – ready to fire
# fire – bullet is firing
bulletstate = "ready"


# Moving left and right
def move_left():
    # Begin player at x-coordinate
    x = player.xcor()
    x -= playerspeed
    if x < - 280:
        x = - 280
    player.setx(x)


def move_right():
    x = player.xcor()
    x += playerspeed
    if x > 280:
        x = 280
    player.setx(x)


def fire_bullet():
    # change to global if needed
    global bulletstate
    if bulletstate == "ready":
        bulletstate = "fire"
        bullet.setposition(player.position())
        bullet.forward(50)
        bullet.showturtle()


def isCollision(t1, t2):
    distance = math.sqrt(math.pow(
        t1.xcor() - t2.xcor(), 2) + math.pow(t1.ycor() - t2.ycor(), 2))
    if distance < 15:
        return True
    else:
        return False

    # bullet barely on top of player
    x = player.xcor()
    y = player.ycor() + 10
    bullet.setposition(x, y)
    bullet.showturtle()


def move():
    global enemyspeed, bullletstate

    wn.tracer(False)

    for enemy in enemies:
        x = enemy.xcor() + enemyspeed

        enemy.setx(x)

        if isCollision(player, enemy):
            player.hideturtle()
            enemy.hideturtle()
            print("Game Over")
            wn.update()
            return

        if isCollision(bullet, enemy):
            # reset the bullet
            bullet.hideturtle()
            bullet.setposition(0, 600)
            bulletstate = "ready"

            # reset the enemy
            enemy.hideturtle()
            x = random.randint(-200, 200)
            y = random.randint(100, 250)
            enemy.setposition(x, y)
            enemy.showturtle()

        if bulletstate == "fire":
            bullet.forward(bulletspeed)

        wn.tracer(True)

        wn.ontime(move, 25)


# Binding keys
wn.onkey(move_left, "a")
wn.onkey(move_right, "d")
wn.onkey(fire_bullet, "\\")
wn.listen()

# Move the enemy
move()

# main game loop
while True:
    # Move bullet
    if bulletstate == "fire":
        y = bullet.ycor()
        y += bulletspeed
        bullet.sety(y)

    # Ensure bullet reaches top
    if bullet.ycor() > 275:
        bullet.hideturtle()
        bulletstate = "ready"
    wn.tracer(True)

    wn.ontimer(move, 25)


relay = input("Press enter to finish.")
